import React from 'react';
import { View, Text, Button, StyleSheet, StatusBar } from 'react-native';
import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";
import HomeScreen from "./src/HomeScreen"; 
import UsersScreen from "./src/Userscreen"; 
import LoginScreen from "./src/LoginScreen"; 
import CadastroScreen from "./src/CadastroScreen";


const Stack = createStackNavigator();

const Home = ({ navigation }) => {
  return (
    <View style={styles.container}>
      <Text style={styles.title}></Text>
      <View style={styles.buttonContainer}>
        <Button title="Horários Disponíveis" onPress={() => alert('Horários Disponíveis')} />
        <Button title="Salas Disponíveis" onPress={() => alert('Salas Disponíveis')} />
        <Button title="Dias Disponíveis" onPress={() => alert('Dias Disponíveis')} />
        <Button title="Ver Usuários" onPress={() => navigation.navigate("Users")} />
      </View>
    </View>
  );
};

const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator initialRouteName="Home">
        <Stack.Screen name="Home" component={HomeScreen} />
        <Stack.Screen name="Users" component={UsersScreen} />
        <Stack.Screen name="Login" component={LoginScreen} />
        <Stack.Screen name="Cadastro" component={CadastroScreen} />
      </Stack.Navigator>
    </NavigationContainer>
  )
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontSize: 24,
    marginBottom: 20,
  },
  buttonContainer: {
    width: '80%',
    justifyContent: 'space-around',
    height: '20%',
  },
});

export default App;
