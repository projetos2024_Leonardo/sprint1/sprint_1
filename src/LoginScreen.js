import React, { useState } from 'react';
import { View, Text, Button, TextInput, StyleSheet, TouchableOpacity, FlatList } from 'react-native';
import { useNavigation } from '@react-navigation/native';

const LoginScreen = ({ onLogin }) => {
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
  const [error, setError] = useState('');

  const navigation = useNavigation();

  const handleUsers = () => {
    navigation.navigate("Users");
  };

  const handleLogin = () => {
    if (username === 'leo' && password === '123') {
      navigation.navigate('Users');
    } else {
      setError('Usuário ou senha incorretos');
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Faça Login</Text>
      <TextInput
        style={styles.input}
        placeholder="Usuário"
        onChangeText={(text) => setUsername(text)}
        value={username}
      />
      <TextInput
        style={styles.input}
        placeholder="Senha"
        onChangeText={(text) => setPassword(text)}
        value={password}
        secureTextEntry
      />
      {error ? <Text style={styles.error}>{error}</Text> : null}
      <Button title="Entrar" onPress={handleLogin} />
    </View>
  );
};

const HomeScreen = () => {
 
  const [loggedIn, setLoggedIn] = useState(false);

  const handleLogin = () => {
    setLoggedIn(true);
  };

  const [salasDeAula, setSalasDeAula] = useState([
    { id: 1, nome: 'Sala 30', capacidade: 32, disponivel: true, localizacao: 'Biblioteca' },
    { id: 2, nome: 'Sala 31', capacidade: 32, disponivel: false, localizacao: 'Laboratorio' },
    { id: 3, nome: 'Sala 32', capacidade: 32, disponivel: true, localizacao: 'Sala de musica' },
  ]);

  const reservarSala = (id) => {
    
    const salaReservada = salasDeAula.find(sala => sala.id === id);
  
    
    if (salaReservada && salaReservada.disponivel) {
      
      const novasSalasDeAula = [...salasDeAula];
      
      const index = novasSalasDeAula.findIndex(sala => sala.id === id);
     
      novasSalasDeAula[index] = { ...novasSalasDeAula[index], disponivel: false };
      
      setSalasDeAula(novasSalasDeAula);
  
      console.log(`Sala ${salaReservada.nome} reservada!`);
    } else {
      console.log(`Sala ${id} não está disponível para reserva.`);
    }
  };
  

  const renderSalaDeAula = ({ item }) => (
    <TouchableOpacity onPress={() => reservarSala(item.id)} style={styles.itemContainer}>
      <Text style={styles.salaNome}>{item.nome}</Text>
      <Text>Capacidade: {item.capacidade}</Text>
      <Text>Localização: {item.localizacao}</Text>
      <Text style={{ color: item.disponivel ? 'green' : 'red' }}>
        {item.disponivel ? 'Disponível' : 'Não disponível'}
      </Text>
    </TouchableOpacity>
  );

 

  if (!loggedIn) {
    return <LoginScreen onLogin={handleLogin} />;
  }

  return (
    <View style={styles.container}>
      <Text style={styles.title}>Salas de Aula Disponíveis</Text>
      <FlatList
        data={salasDeAula}
        renderItem={renderSalaDeAula}
        keyExtractor={(item) => item.id.toString()}
        style={styles.listaSalas}
      />
      <Button title="Ver Usuários" onPress={handleUsers} />
    </View>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingHorizontal: 20,
    paddingTop: 40,
    backgroundColor: 'white', 
  },
  title: {
    fontSize: 24,
    fontWeight: 'bold',
    marginBottom: 20,
    color: 'white', 
  },
  listaSalas: {
    flex: 1,
  },
  itemContainer: {
    padding: 20,
    marginBottom: 10,
    backgroundColor: '#ffffff', 
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#ccc',
  },
  salaNome: {
    fontSize: 18,
    fontWeight: 'bold',
    marginBottom: 5,
    color: '#00447c', 
  },
  input: {
    height: 40,
    width: '80%',
    borderColor: '#00447c', 
    borderWidth: 1,
    marginBottom: 20,
    paddingHorizontal: 10,
  },
  error: {
    color: 'red',
    marginBottom: 10,
  },
});

export default LoginScreen;
