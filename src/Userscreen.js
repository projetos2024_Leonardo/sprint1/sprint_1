import React, { useEffect, useState } from "react";
import { View, Text, FlatList, Button } from "react-native";
import axios from "axios";

const UsersScreen = () => {
  const [users, setUsers] = useState([]);
  const [reservations, setReservations] = useState([]);

  const availableRooms = [
    { id: 1, name: "Sala 1", capacity: 32 },
    { id: 2, name: "Sala 2", capacity: 32 },
    { id: 3, name: "Sala 3", capacity: 32 },
  ];

  const reserveRoom = () => {
    const roomId = availableRooms[0].id;
    const room = availableRooms.find((room) => room.id === roomId);
    if (room) {
      setReservations(prevReservations => [...prevReservations, room]);
    }
  };

  return (
    <View>
      <Text>Lista de usuários</Text>
      <FlatList
        data={users}
        keyExtractor={(item) => item.id.toString()}
        renderItem={({ item }) => (
          <View>
            <Text>Nome: {item.name}</Text>
          </View>
        )}
      />
      <Button title="Reservar Sala" onPress={reserveRoom} />
      <View>
        <Text>Reservas:</Text>
        {reservations.map((room, index) => (
          <Text key={index}>Reserva {index + 1}: Sala {room.name}</Text>
        ))}
      </View>
    </View>
  );
};

export default UsersScreen;
